Assignment 8
------------

- Make estimate of mean
- Make estimate of median
- Approximate Poisson distribution using normal distribution (lambda rate Poisson, lambda mean and variance normal)
